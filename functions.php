<?php

/**
 * Insert custom and third party styles and scripts into index page
 */

require dirname(__FILE__) . "/functions/assets.php";
require dirname(__FILE__) . "/functions/utilities.php";

require dirname(__FILE__) . "/functions/default-options.php";
require dirname(__FILE__) . "/functions/hooks-functions.php";
require dirname(__FILE__) . "/functions/hooks.php";
require dirname(__FILE__) . "/functions/filters.php";
require dirname(__FILE__) . "/functions/register-sidebar.php";
require dirname(__FILE__) . "/functions/shortcodes/_init.php";
