;
(function ($) {
  var productCatalogPhoto = $('[data-loop-before-title]');
  var productCatalogActionRow = $('.product-cut__action-row');
  var productCatalogPhotoHeight = productCatalogPhoto.innerHeight();

  $(window).resize(function () {
    productCatalogActionRow.css('top', productCatalogPhotoHeight - 40);
  })


})(jQuery);