;
(function ($) {


  $('[data-slider="banner-slider"]').each(function () {

    var scope = $(this);
    var slider = scope.find('[data-slider-slides]');
    var current = scope.find('[data-slider-current]');
    var total = scope.find('[data-slider-total]');
    console.log(current);

    // var responsiveCols = slider.attr('data-slider-slides');

    /* Exit if slider is already initialized after Ajax request */
    if (typeof event !== 'undefined' && event.type === 'ajaxComplete' && slider.hasClass('slick-initialized')) {
      return;
    }
    slider.on('init', function (event, slick) {
      current.text(slick.currentSlide + 1);
      total.text(slick.slideCount);
    })



    slider.slick({
      dots: false,
      arrows: true,
      infinite: true,
      adaptiveHeight: true,
      slidesToShow: 1,
      autoplay: true,
      autoplaySpeed: 3000,
      swipeToSlide: true,
      mobileFirst: true,
      rows: 1,
      prevArrow: scope.find('[data-slider-arrow-left]').removeClass('hidden'),
      nextArrow: scope.find('[data-slider-arrow-right]').removeClass('hidden'),
      responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 2,
            autoplay: false,
          }
        },
      ]
    });

    slider.on('afterChange', function () {
      $.mlsLazzyload.update();
    });
    slider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
      current.text(nextSlide + 1);
    });
  });


  $('[data-slider="secondary-slider"]').each(function () {

    var scope = $(this);
    var slider = scope.find('[data-slider-slides]');
    var current = scope.find('[data-slider-current]');
    var total = scope.find('[data-slider-total]');
    console.log(current);

    // var responsiveCols = slider.attr('data-slider-slides');

    /* Exit if slider is already initialized after Ajax request */
    if (typeof event !== 'undefined' && event.type === 'ajaxComplete' && slider.hasClass('slick-initialized')) {
      return;
    }
    slider.on('init', function (event, slick) {
      current.text(slick.currentSlide + 1);
      total.text(slick.slideCount);
    })



    slider.slick({
      dots: false,
      arrows: true,
      infinite: true,
      adaptiveHeight: true,
      slidesToShow: 1,
      autoplay: false,
      autoplaySpeed: 3000,
      swipeToSlide: true,
      mobileFirst: true,
      rows: 1,
      centerMode: true,
      prevArrow: scope.find('[data-slider-arrow-left]').removeClass('hidden'),
      nextArrow: scope.find('[data-slider-arrow-right]').removeClass('hidden'),
      responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 4
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 2
          }
        }
      ]
    });

    slider.on('afterChange', function () {
      $.mlsLazzyload.update();
    });
    slider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
      current.text(nextSlide + 1);
    });
  });

  $('[data-slider="slider-third"]').each(function () {

    var scope = $(this);
    var slider = scope.find('[data-slider-slides]');
    var current = scope.find('[data-slider-current]');
    var total = scope.find('[data-slider-total]');
    console.log(current);

    // var responsiveCols = slider.attr('data-slider-slides');

    /* Exit if slider is already initialized after Ajax request */
    if (typeof event !== 'undefined' && event.type === 'ajaxComplete' && slider.hasClass('slick-initialized')) {
      return;
    }
    slider.on('init', function (event, slick) {
      current.text(slick.currentSlide + 1);
      total.text(slick.slideCount);
    })



    slider.slick({
      dots: false,
      arrows: true,
      infinite: true,
      adaptiveHeight: true,
      slidesToShow: 1,
      autoplay: false,
      autoplaySpeed: 3000,
      swipeToSlide: true,
      mobileFirst: true,
      rows: 1,
      centerMode: true,
      prevArrow: scope.find('[data-slider-arrow-left]').removeClass('hidden'),
      nextArrow: scope.find('[data-slider-arrow-right]').removeClass('hidden'),
      responsive: [
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 2
          }
        }
      ]
    });

    slider.on('afterChange', function () {
      $.mlsLazzyload.update();
    });
    slider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
      current.text(nextSlide + 1);
    });
  });

  $('[data-slider="slider-simple"]').each(function () {

    var scope = $(this);
    var slider = scope.find('[data-slider-slides]');
    var current = scope.find('[data-slider-current]');
    var total = scope.find('[data-slider-total]');
    console.log(current);

    // var responsiveCols = slider.attr('data-slider-slides');

    /* Exit if slider is already initialized after Ajax request */
    if (typeof event !== 'undefined' && event.type === 'ajaxComplete' && slider.hasClass('slick-initialized')) {
      return;
    }
    slider.on('init', function (event, slick) {
      current.text(slick.currentSlide + 1);
      total.text(slick.slideCount);
    })



    slider.slick({
      dots: false,
      arrows: true,
      infinite: true,
      adaptiveHeight: true,
      slidesToShow: 1,
      autoplay: false,
      autoplaySpeed: 3000,
      swipeToSlide: true,
      mobileFirst: true,
      rows: 1,
      centerMode: true,
      prevArrow: scope.find('[data-slider-arrow-left]').removeClass('hidden'),
      nextArrow: scope.find('[data-slider-arrow-right]').removeClass('hidden'),
      responsive: [
        {
          breakpoint: 1199,
          settings: {
            slidesToShow: 5
          }
        },
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 4
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 2
          }
        }
      ]
    });

    slider.on('afterChange', function () {
      $.mlsLazzyload.update();
    });
    slider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
      current.text(nextSlide + 1);
    });
  });



})(jQuery);