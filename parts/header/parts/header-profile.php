<div class="user-panel__item">
        <span class="user-panel__link">
            <i class="user-panel__ico user-panel__ico--profile">
                <?php premmerce_the_svg('user'); ?>
            </i>
        </span>
    <div class="user-panel__drop user-panel__drop--rtl">
        <?php get_template_part('parts/header/parts/header', 'profile-overlay'); ?>
    </div>
</div>