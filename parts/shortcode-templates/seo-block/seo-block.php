<section class="seo-block">

  <div class="seo-block__row">

  <?php foreach($fields as $field) : 

    if ($field['seo-block-first-image']) : ?>
      <div class="seo-block__image">
        <img src="<?php echo $field['seo-block-first-image']['url'] ?>" alt="<?php echo $field['seo-block-title'] ?>"/>  
      </div>
      <div class="seo-block__image">
        <img src="<?php echo $field['seo-block-second-image']['url'] ?>" alt="<?php echo $field['seo-block-title'] ?>"/>
      </div>
      <div class="seo-block__descr">
        <h3 class="seo-block__title"><?php echo $field['seo-block-title'] ?></h3>
        <p class="seo-block__text"><?php echo $field['seo-block-text'] ?></p>
      </div>

    <?php endif; endforeach; ?>

  </div>

</section>

