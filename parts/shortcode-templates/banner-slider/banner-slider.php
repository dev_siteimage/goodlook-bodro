

<section class="advantages">

<ul class="advantages__row">

    <?php foreach($fields as $field) : 

        if ($field['image']) : ?>

            <li class="advantages__item">
              <img src="<?php echo $field['image']['url'] ?>" alt=""/>

            </li>

    <?php endif; endforeach; ?>

</ul>

</section>

