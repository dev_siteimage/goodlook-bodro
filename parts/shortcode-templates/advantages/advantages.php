

<section class="advantages">

    <ul class="advantages__row">

        <?php foreach($fields as $field) : 

            if ($field['counter']) : ?>

                <li class="advantages__item">
                    <div class="advantage-item">
                        <div class="advantage-item__ico">
                            <img src="<?php echo $field['image']['url'] ?>" alt="<?php echo $field['title'] ?>"/>
                        </div>
                        <div class="advantage-item__content">
                            <div class="advantage-item__counter">
                                <span class="advantage-item__counter-number"><?php echo $field['counter'] ?></span>
                                <span class="advantage-item__counter-divider">/</span>
                                <span class="advantage-item__counter-total">5</span>
                            </div>
                            <h3 class="advantage-item__title"><?php echo $field['title'] ?></h3>
                            <p class="advantage-item__descr"><?php echo $field['description'] ?></p>
                        </div>
                    </div>

                </li>

        <?php endif; endforeach; ?>

    </ul>

</section>

