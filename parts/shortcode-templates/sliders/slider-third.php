<section class="slider-third" data-slider="<?php echo($indicator); ?>">
  <div class="slider-third__row" data-slider-slides >

    <?php foreach($fields as $field) : 
        if ($field['image']) : ?>

          <a href="<?php echo $field['url'] ?>" class="slider-third__item">
            <img src="<?php echo $field['image']['url'] ?>" alt=""/>

            <?php if ($field['image']) : ?>
              <div class="slider-third__item-container"> 
                <span class="slider-third__text"><?php echo $field['text'] ?></span>
                <span class="slider-third__title"><?php echo $field['title'] ?></span>
              </div> 
            <?php endif; ?>

            </a>

        <?php endif; endforeach; ?>
  </div>

  <div class="slider-navigation slider-navigation--secondary slider-navigation--third">
    <span class="slider-navigation__name"><?php echo $slider_title ?></span>
    <div class="slider-navigation__elements-container">
      <div class="slider-navigation__arrow slider-navigation__arrow--left hiiden" data-slider-arrow-left>
          <?php premmerce_the_svg('arrow-long-left') ?>
      </div>
      <div class="slider-navigation__counter">
        <span class="slider-navigation__current" data-slider-current>
        </span>
        <span class="slider-navigation__divider">/</span>
        <span class="slider-navigation__total" data-slider-total>
        </span>
      </div>
      <div class="slider-navigation__arrow slider-navigation__arrow--right hiiden"  data-slider-arrow-right>
          <?php premmerce_the_svg('arrow-long-right') ?>
      </div>
    </div>
  </div>

</section>

