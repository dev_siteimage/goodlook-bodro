<section class="slider-simple" data-slider="<?php echo($indicator); ?>">
  <div class="slider-simple__row" data-slider-slides >

    <?php foreach($fields as $field) : 
        if ($field['image']) : ?>

          <a href="<?php echo $field['url'] ?>" class="slider-simple__item">
            <img src="<?php echo $field['image']['url'] ?>" alt=""/>

            <?php if ($field['image']) : ?>
              <span class="slider-simple__title"><?php echo $field['title'] ?></span>
              <span class="slider-simple__text"><?php echo $field['text'] ?></span>
            <?php endif; ?>

            </a>

        <?php endif; endforeach; ?>
  </div>

  <div class="slider-navigation slider-navigation--secondary slider-navigation--simple">
  <span class="slider-navigation__name">#<span><?php echo $slider_title ?></span></span>
    <div class="slider-navigation__elements-container">
      <div class="slider-navigation__arrow slider-navigation__arrow--left hiiden" data-slider-arrow-left>
          <?php premmerce_the_svg('arrow-long-left') ?>
      </div>
      <div class="slider-navigation__arrow slider-navigation__arrow--right hiiden"  data-slider-arrow-right>
          <?php premmerce_the_svg('arrow-long-right') ?>
      </div>
    </div>
  </div>

</section>

