<section class="site-slider site-slider--<?php echo($modificator); ?>" data-slider="<?php echo($indicator); ?>">
  <div class="site-slider__row" data-slider-slides >
    <?php foreach($fields as $field) : 
        if ($field['image']) : ?>
          <div class="site-slider__item">
            <img src="<?php echo $field['image']['url'] ?>" alt=""/>
          </div>
        <?php endif; endforeach; ?>
  </div>

  <h1 class="site-slider__title">
    <span><?php echo $slider_title ?></span>
    <span><?php echo $slider_title_two ?></span>
  </h1>

  <div class="slider-navigation slider-navigation--main">
    <div class="slider-navigation__arrow slider-navigation__arrow--left hiiden" data-slider-arrow-left>
        <?php premmerce_the_svg('arrow-long-left') ?>
    </div>
    <div class="slider-navigation__counter">
      <span class="slider-navigation__current" data-slider-current>
      </span>
      <span class="slider-navigation__divider">/</span>
      <span class="slider-navigation__total" data-slider-total>
      </span>
    </div>
    <div class="slider-navigation__arrow slider-navigation__arrow--right hiiden"  data-slider-arrow-right>
        <?php premmerce_the_svg('arrow-long-right') ?>
    </div>
  </div>

</section>

