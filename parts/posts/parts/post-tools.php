<div class="post-tools">
    <div class="post-tools__item">
        <time class="post-tools__value" datetime="<?php the_time('d-m-Y') ?>"><?php the_time(get_option( 'date_format' )); ?></time>
    </div>
</div>