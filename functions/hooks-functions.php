<?php

/**
 * Define theme hooks functions
 */

/**
 * Additional info for quick view single product
 */

if (!function_exists('premmerce_single_product_add_info')) {
  function premmerce_single_product_add_info()
  {
    global $product;
    if (has_action('woocommerce_product_additional_information')) {
      do_action('woocommerce_product_additional_information', $product);
    }
  }
}


/**
 * Sku number for quick view
 * repeats sku part of saleszone/woocommerce/single-product/meta.php
 */

if (!function_exists('premmerce_single_product_get_sku')) {
  function premmerce_single_product_get_sku()
  {
    global $product;
    if (wc_product_sku_enabled() && ($product->get_sku() || $product->is_type('variable'))) {
      $sku = $product->get_sku() ? $product->get_sku() : esc_html__('N/A', 'saleszone');
      echo '<span class="sku _wrapper">' . esc_html__('SKU:', 'saleszone') . '&nbsp;<span data-product-sku>' . $sku . '</span></span>';
    }
  }
}


/**
 * Brand link for single product
 */

if (!function_exists('premmerce_get_brand_link')) {
  function premmerce_get_brand_link()
  {
      if(function_exists('premmerce_get_product_brand')){
          global $product;

          if($product){
              $id = $product->get_ID();
              $brand = premmerce_get_product_brand($id);
              if ($brand) {
                  echo '<div class="brand-link"><a class="link link--secondary" href="' . get_term_link($brand->slug, 'product_brand') . '">' . $brand->name . '</a></div>';
              }
          }
      }
  }
}

/**
 * Brand option for cart product
 */

if (!function_exists('premmerce_cart_get_brand_link')) {
  function premmerce_cart_get_brand_link($product)
  {
      if(function_exists('premmerce_get_product_brand')){
          $product_type = $product->get_type();

          if($product_type == 'variation'){
              $id = $product->get_parent_id();
          }else{
              $id = $product->get_id();
          }

          $brand = premmerce_get_product_brand($id);

          if ($brand) {
              echo '<div class="cart-product__option">';
              echo '<span class="cart-product__option-key">' . __('Brand', 'saleszone') . ': ' . '</span>';
              echo '<a class="cart-product__option-value" href="' . get_term_link($brand->slug, 'product_brand') . '">' . $brand->name . '</a></div>';
          }
      }
  }
}

/**
 * Catalog title
 */

if(!function_exists('premmerce_catalog_title')){
  function premmerce_catalog_title($template_name){
    if ( $template_name == 'loop/products-layout.php' ){
        ?>

        <div class="content__header content__header--category">
            <h1 class="content__title">
                <?php woocommerce_page_title(); ?>
            </h1>
            <?php if(woocommerce_get_loop_display_mode() != 'both'): ?>
                <?php wc_get_template('loop/toolbar.php'); ?>
            <?php endif; ?>
        </div>
        <?php
    }
  }
}

/**
 * Catalog header
 */

if(!function_exists('premmerce_render_catalog_header_brand')){
  function premmerce_render_catalog_header_brand(){
      if(!is_tax('product_brand')){
          return;
      }
      ?>
      <div class="pc-category-products-layout__header-brand">
          <div class="row row--ib row--vindent-s">
              <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                  <?php premmerce_brand_sidebar_image(); ?>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
                  <?php if($description = premmerce_archive_description()) :?>
                      <div class="typo">
                          <?php echo $description; ?>
                      </div>
                  <?php endif; ?>
              </div>
          </div>
      </div>
      <?php
  }
}


/**
 * Catalog description
 */

if(!function_exists('premmerce_catalog_footer')){
  function premmerce_catalog_footer(){
    if(!is_tax('product_brand')){
    wc_get_template('loop/archive-description.php');
    }
  }
}
