<?php

add_shortcode('seo_block', function ($atts){

    $atts = shortcode_atts( array(
        'title'      => '',
        'template'   => 'seo-block',
        'limit'      => '20',
        'orderby'    => 'name',
        'order'      => 'ASC',
        'columns'    => '5',
        'hide_empty' => 1,
        'parent'     => '',
        'ids'        => '',
    ), $atts, 'seo_block' );

    $fields = get_field('seo-block-item');
    // var_dump($fields);

    $columns = absint( $atts['columns'] );

    wc_set_loop_prop( 'columns', $columns );

    ob_start();

    wc_get_template('../parts/shortcode-templates/seo-block/' . $atts['template'] . '.php',[
        'attributes' => $atts,
        'fields' => $fields,
    ]);

    return ob_get_clean();

});