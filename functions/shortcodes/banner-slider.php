<?php

add_shortcode('banner_slider', function ($atts){

    $atts = shortcode_atts( array(
        'id'         => null,
        'post_type' => 'sliders',
        'title'      => '',
        'template'   => '',
        'limit'      => '20',
        'orderby'    => 'name',
        'order'      => 'ASC',
        'columns'    => '5',
        'hide_empty' => 1,
        'parent'     => '',
        'ids'        => '',
    ), $atts, 'banner_slider' );

    $fields = get_field('images', $atts['id']);
    $title = get_field('title', $atts['id']);
    $text = get_field('text', $atts['id']);
    $url = get_field('url', $atts['id']);
    $slider_title = get_field('slider-title', $atts['id']);
    $slider_title_two = get_field('slider-title-two', $atts['id']);
    $indicator = get_field('slider-indificator', $atts['id']);
    $modificator = get_field('slider-modificator', $atts['id']);
    // var_dump($indicator);

    ob_start();

    wc_get_template('../parts/shortcode-templates/sliders/' . $atts['template'] . '.php',[
        'attributes' => $atts,
        'fields' => $fields,
        'indicator' => $indicator,
        'modificator' => $modificator,
        'title' => $title,
        'text' => $text,
        'slider_title' => $slider_title,
        'slider_title_two' => $slider_title_two,
    ]);

    return ob_get_clean();

});