<?php

add_shortcode('advantages_s', function ($atts){

    $atts = shortcode_atts( array(
        'title'      => '',
        'template'   => 'advantages',
        'limit'      => '20',
        'orderby'    => 'name',
        'order'      => 'ASC',
        'columns'    => '5',
        'hide_empty' => 1,
        'parent'     => '',
        'ids'        => '',
    ), $atts, 'advantages_s' );

    // Get terms and workaround WP bug with parents/pad counts.

    $args = array(
        'orderby'    => $atts['orderby'],
        'order'      => $atts['order'],
        'hide_empty' => $hide_empty,
        'include'    => $ids,
        'pad_counts' => true,
        'child_of'   => $atts['parent'],
		'hierarchical' => false,
		'number'   => (int)$atts['limit'],
    );

    // $product_categories = get_terms( 'product_cat', $args );

    $fields = get_field('advantages-item');
    // var_dump($fields);


    $columns = absint( $atts['columns'] );

    wc_set_loop_prop( 'columns', $columns );

    ob_start();

    wc_get_template('../parts/shortcode-templates/advantages/' . $atts['template'] . '.php',[
        'attributes' => $atts,
        'fields' => $fields,
    ]);

    return ob_get_clean();

});