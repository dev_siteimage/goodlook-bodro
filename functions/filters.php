<?php

/**
 * Replace parent theme svg icons
 */
$replaced_icons = [
    'heart',
    'star',
    'angle-left',
    'angle-right',
    'arrow-down',
    'arrow-big-right',
    'arrow-big-left',
    'calendar',
    'comment',
    'compare',
    'delete',
    'user',
    'move',
    'cart-bag',
    'write',
    'order',
    'delivery',
    'user-order',
    'payment',
    'arrow-long-left',
    'arrow-long-right',
];

foreach ($replaced_icons as $icon_name){
    add_filter('premmerce-svg-icon--'. $icon_name, function ($data) use ($icon_name){
        return [
            'html' => child_theme_get_svg($icon_name, $data['class']),
            'class' => $data['class']
        ];
    });
}

/**
 * Removes seo text in footer
 */
add_filter('premmerce-homepage-footer-seo-text', function(){
    return false;
});

/**
 * Remove product labels svg
 */

add_filter('premmerce-svg-icon--new', function ($data){
    return [
      'html' => ''
    ];
});


add_filter('product-photo-data-slider-slides', function(){
    return '4,3,3,4';
});

/**
 * Remove tab accessories
 */
add_filter('woocommerce_product_tabs', function($tabs){
    unset($tabs['accessories']);

    return $tabs;
}, 30);


add_filter('woocommerce_output_product_categories_args-cat-layout', 'premmerce_filter_output_categories_args');
add_filter('woocommerce_output_product_categories_args-product-layout', 'premmerce_filter_output_categories_args');
function premmerce_filter_output_categories_args($args){

    $args['before'] = '<div class="content__row"><div class="woocommerce columns-3"><div class="grid-list">';
    $args['after'] = '</div></div></div>';

    return $args;
}

add_filter('premmerce-myaccount-edit-address-column-class', function (){
    return 'col-md-6 col-xs-12 col-sm-12';
});

add_filter('gettext', function($text) {
    if( $text == 'View in Cart' ) {
       return 'Купить';
   }
   else return $text;
});
add_filter('gettext', function($text) {
    if( $text == 'Details' ) {
       return 'Подробнее';
   }
   else return $text;
});
add_filter('gettext', function($text) {
    if( $text == 'Edit cart' ) {
       return 'Подробнее';
   }
   else return $text;
});
add_filter('gettext', function($text) {
    if( $text == 'Payment Details' ) {
       return 'Детали Оплаты';
   }
   else return $text;
});

/**
 * Wihslit and checkout button type
 */
add_filter('premmerce_comparison_catalog_button_type', 'premmerce_filter_action_buttons_type', 1);
add_filter('premmerce_comparison_catalog_snippet_button_type', 'premmerce_filter_action_buttons_type');
add_filter('premmerce_comparison_product_button_type', 'premmerce_filter_action_buttons_type');

add_filter('premmerce_wishlist_catalog_button_type', 'premmerce_filter_action_buttons_type', 1);
// add_filter('premmerce_wishlist_catalog_snippet_button_type', 'premmerce_filter_action_buttons_type');
// add_filter('premmerce_wishlist_product_button_type', 'premmerce_filter_action_buttons_type');

function premmerce_filter_action_buttons_type($type){
    return 'button';
}

add_filter('woocommerce_billing_fields', 'custom_woocommerce_billing_fields');

function custom_woocommerce_billing_fields($fields)
{

    $fields['billing_all_name'] = array(
        'label' => __('ФИО', 'woocommerce'), 
        'placeholder' => _x('', 'placeholder', 'woocommerce'), 
        'required' => true, 
        'clear' => false, 
        'type' => 'text', 
        'class' => array('my-css')   
    );

    return $fields;
}

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

function custom_override_checkout_fields( $fields ) {
     unset($fields['billing']['billing_first_name']);
	 unset($fields['billing']['billing_last_name']);
	 unset($fields['billing']['billing_company']);
	 unset($fields['billing']['billing_address_2']);
	 unset($fields['billing']['billing_address_1']);
	 unset($fields['billing']['billing_state']);
	 unset($fields['billing']['billing_postcode']);
     return $fields;
} 

add_filter( 'woocommerce_checkout_fields', 'custom_woocommerce_billing_fields_order' );
 
function custom_woocommerce_billing_fields_order( $checkout_fields ) {
	$checkout_fields['billing']['billing_all_name']['priority'] = 10;
	$checkout_fields['billing']['billing_country']['priority'] = 20;
	$checkout_fields['billing']['billing_email']['priority'] = 30;
	$checkout_fields['billing']['billing_city']['priority'] = 40;
	$checkout_fields['billing']['billing_phone']['priority'] = 50;
	return $checkout_fields;
}
add_filter('woocommerce_shipping_fields', 'custom_woocommerce_shipping_fields');

function custom_woocommerce_shipping_fields($fields)
{

    $fields['shipping_all_name'] = array(
        'label' => __('ФИО', 'woocommerce'), 
        'placeholder' => _x('', 'placeholder', 'woocommerce'), 
        'required' => true, 
        'clear' => false, 
        'type' => 'text', 
        'class' => array('my-css')   
    );
    $fields['shipping_email'] = array(
        'label' => __('Email', 'woocommerce'), 
        'placeholder' => _x('', 'placeholder', 'woocommerce'), 
        'required' => true, 
        'clear' => false, 
        'type' => 'text', 
        'class' => array('my-css')   
    );
    $fields['shipping_phone'] = array(
        'label' => __('Телефон', 'woocommerce'), 
        'placeholder' => _x('', 'placeholder', 'woocommerce'), 
        'required' => true, 
        'clear' => false, 
        'type' => 'text', 
        'class' => array('my-css')   
    );

    return $fields;
}

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields_shipping' );

function custom_override_checkout_fields_shipping( $fields ) {
     unset($fields['shipping']['shipping_first_name']);
	 unset($fields['shipping']['shipping_last_name']);
	 unset($fields['shipping']['shipping_company']);
	 unset($fields['shipping']['shipping_address_2']);
	 unset($fields['shipping']['shipping_address_1']);
	 unset($fields['shipping']['shipping_state']);
	 unset($fields['shipping']['shipping_postcode']);
     return $fields;
} 

add_filter( 'woocommerce_checkout_fields', 'custom_woocommerce_shipping_fields_order' );
 
function custom_woocommerce_shipping_fields_order( $checkout_fields ) {
	$checkout_fields['shipping']['shipping_all_name']['priority'] = 10;
	$checkout_fields['shipping']['shipping_country']['priority'] = 20;
	$checkout_fields['shipping']['shipping_email']['priority'] = 30;
	$checkout_fields['shipping']['shipping_city']['priority'] = 40;
	$checkout_fields['shipping']['shipping_phone']['priority'] = 50;
	return $checkout_fields;
}

//Footer menu

function register_footer_menu() {
    register_nav_menu('footer-menu',__( 'Footer Menu' ));
  }
  add_action( 'init', 'register_footer_menu' );

  /*Creating shortcode for menu*/
function list_menu($atts, $content = null) {
    extract(shortcode_atts(array(  
        'menu'            => '', 
        'container'       => '', 
        'container_class' => '', 
        'container_id'    => '', 
        'menu_class'      => '', 
        'menu_id'         => '',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'depth'           => 0,
        'walker'          => '',
        'theme_location'  => ''), 
        $atts));
   
    return wp_nav_menu( array( 
        'menu'            => $menu, 
        'container'       => $container, 
        'container_class' => $container_class, 
        'container_id'    => $container_id, 
        'menu_class'      => $menu_class, 
        'menu_id'         => $menu_id,
        'echo'            => false,
        'fallback_cb'     => $fallback_cb,
        'before'          => $before,
        'after'           => $after,
        'link_before'     => $link_before,
        'link_after'      => $link_after,
        'depth'           => $depth,
        'walker'          => $walker,
        'theme_location'  => $theme_location));
}
add_shortcode("listmenu", "list_menu");


//Slider post typs

// Our custom post type function
function create_slider_posttype() {
 
    register_post_type( 'sliders',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Sliders' ),
                'singular_name' => __( 'Slider' )
            ),
            'public' => true,
            'has_archive' => true,
            'show_in_rest' => true,
 
        )
    );
}
// Hooking up our function to theme setup
add_action( 'init', 'create_slider_posttype' );