<?php



add_action("wp_enqueue_scripts", function (){
    $uri = get_stylesheet_directory_uri();
    $theme = wp_get_theme();
    $version = $theme->get('Version');

    wp_enqueue_style("saleszone-child-styles", $uri . "/public/css/styles.min.css", ['saleszone-styles'], $version);
    wp_enqueue_script("slick", $uri . "/public/js/slick.js",  array('jquery'), $version, true);
    wp_enqueue_script("saleszone-product", $uri . "/public/js/product-cut.js", ['saleszone-scripts'], $version, true);
    wp_enqueue_script("saleszone-site-slider", $uri . "/public/js/site-slider.js", ['saleszone-scripts'], $version, true);
});

