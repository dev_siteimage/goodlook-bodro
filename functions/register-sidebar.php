<?php

/**
 * Override SalesZone widgets area
 */

add_action('widgets_init', function () {

  premmerce_register_sidebar([
    'name' => 'Front Page widgets',
    'id' => 'homepage_widgets',
    'description' => 'Widgets bar on Front page',
    'before_widget' => '<div class="start-page-glook__widgets-col" id="%1$s"><div class="pc-section-secondary">',
    'after_widget' => '</div></div>',
    'before_title' => '<div class="pc-section-secondary__header">',
    'after_title' => '</div>'
  ]);

});