<?php

global $product;

?>

<button
	class="btn-product-quick-view"
	data-product-id="<?php echo $product->get_id() ?>"
    data-product-quick-view-button="<?php echo esc_attr(json_encode([
        'productId' => $product->get_id()
    ])); ?>"
    title="<?php echo __( 'Quick View', 'saleszone' ) ?>"
    >
  <?php echo __( 'Quick View', 'saleszone' ) ?>
</button>