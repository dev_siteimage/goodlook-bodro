var gulp = require('gulp');
var minifyCss = require('gulp-clean-css');
var renameFile = require('gulp-rename');
var sass = require('gulp-sass');
var combineMq = require('gulp-combine-mq');
var autoprefixer = require('gulp-autoprefixer');
var jsmin = require('gulp-uglify');
//var mainBowerFiles = require('main-bower-files');
var addsrc = require('gulp-add-src');
var sprite = require('gulp.spritesmith');
var changed = require('gulp-changed');
var imagemin = require('gulp-imagemin');
var svgStore = require('gulp-svgstore');
var cheerio = require('gulp-cheerio');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var runSequence = require('run-sequence');

var scssSrc = '_assets/**/*.scss';
var cssDist = 'public/css';

var jsSrc = '_assets/**/*.js';
var jsLocalLibsSrc = '_assets/_js-libs/*.js';
var jsDist = 'public/js';

var imageminSrc = 'uploads/**/*';
var imageminDist = 'uploads_opt';

var svgSpriteSrc = '_assets/svg/sprite/';
var svgSpriteDest = 'public/svg';

/* Call while working with project */
gulp.task('default', ['watch']);

/* Call at first start, after changes in bower.json or in gulp.js */
gulp.task('build', function (cb) {
  runSequence('svg', 'sprite', 'cssbuild', 'libs', 'jsbuild', cb);
});


/* Watch command */
gulp.task('watch', ['css', 'js'], function () {
  gulp.watch(scssSrc, ['css']);
  gulp.watch(jsSrc, ['js']);
  gulp.watch(svgSpriteSrc + '**/*.svg', ['svg']);
});


/* CSS production file packaging */
gulp.task('css', function () {

  /* Bower css libraries. You can config list of files in bower.json */
  //var bowerCss = mainBowerFiles('**/*.css');

  return gulp.src('_assets/final.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({
      precision: 10,
      includePaths: require('node-bourbon').includePaths
    }).on('error', sass.logError))
    //.pipe(addsrc.append(bowerCss))
    .pipe(concat('styles.min.css'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(cssDist));
});


/* CSS production file packaging */
gulp.task('cssbuild', function () {

  /* Bower css libraries. You can config list of files in bower.json */
  //var bowerCss = mainBowerFiles('**/*.css');

  return gulp.src('_assets/final.scss')
    .pipe(sass({
      precision: 10,
      includePaths: require('node-bourbon').includePaths
    }).on('error', sass.logError))
    //.pipe(addsrc.append(bowerCss))
    .pipe(concat('styles.min.css'))
    .pipe(autoprefixer({
      browsers: ['last 4 versions', 'ie > 8', '> 1%']
    }))
    .pipe(combineMq())
    .pipe(minifyCss({'keepSpecialComments': 0}))
    .pipe(gulp.dest(cssDist));
});


/* JS production file packaging */
gulp.task('js', function () {

  /* Packaging all js files into one minified file */
  return gulp.src([jsLocalLibsSrc, jsSrc])
    .pipe(sourcemaps.init())
    .pipe(concat('scripts.min.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(jsDist));
});


/* JS project scripts minimize */
gulp.task('jsbuild', function () {
  return gulp.src([jsLocalLibsSrc, jsSrc])
    .pipe(concat('scripts.min.js'))
    .pipe(jsmin())
    .pipe(gulp.dest(jsDist));
});


/* JS production file packaging */
gulp.task('libs', function () {

  /* Bower js libraries. You can config list of files in bower.json */
  //var bowerJs = mainBowerFiles('**/*.js');

  /* Packaging all js files into one minified file */
  return gulp.src('vendor.min.js')
    .pipe(jsmin())
    .pipe(gulp.dest(jsDist));
});


/* Images production file packaging */
gulp.task('img', function () {
  //var bowerFonts = mainBowerFiles(['**/*.jpg', '**/*.jpeg', '**/*.png', '**/*.gif', '**/*.bmp']);

  return gulp.src()
    .pipe(gulp.dest('public/img'))
});


/* Sprite */
gulp.task('sprite', function () {

  // Generate our spritesheet
  return gulp.src('_assets/icons/img/**/*.png')
    .pipe(sprite({
      imgName: 'sprite.png',
      cssName: 'sprite.scss'
    }))
    .pipe(gulp.dest('public/img'))
    .pipe(gulp.dest('_assets/icons/'));

});


/* Optimize images. Require uploads folder in __assets folder */
gulp.task('imagemin', function () {
  return gulp.src(imageminSrc)
    .pipe(imagemin([
      imagemin.jpegtran({progressive: true}),
      imagemin.optipng({optimizationLevel: 3})
    ]))
    .pipe(gulp.dest(imageminDist));
});

/* Build SVG icons sprite */
gulp.task('svg', function () {
  return gulp.src(svgSpriteSrc + '**/*.svg', {base: svgSpriteSrc})
    .pipe(imagemin([
      imagemin.svgo({
        plugins: [
          {cleanupIDs: true},
          {removeTitle: true},
          {removeDimensions: true},
          {removeViewBox: false},
          {removeStyleElement: true},
          {removeAttrs: {attrs: ['data-name']}}
        ]
      })
    ]))
    .pipe(renameFile({
      prefix: 'svg-icon__'
    }))
    .pipe(svgStore({fileName: 'svg-sprite.svg', inlineSvg: true}))
    .pipe(gulp.dest(svgSpriteDest));
});
